# Diccionari occitan Aspell

Diccionari pel verificador d'ortografia Aspell de GNU.
Conten çò necessari + fichièr de remplaçament e ajuda picada fonetica.

Los fichièrs d’installar son dins « distribution » e los fichièrs per generar lo dico se tròban dins « source ».
Mentre que lo dossièr «oc» servís a bastir lo fichièr Aspell amb lo script d’aquí : https://github.com/GNUAspell/aspell-lang
La lista de mots ven del diccionari Hunspell gràcia a la comanda Unmuch qu'utiliza lo fichièr .aff per crear totas las combinasons possibles (genre, plural, conjugasons).

Lo dico es jos licéncia liura, manquetz pas d'indicar los autors d'origina. Se trapan los obradors dins lo fichièr « info » a la raiç del paquet de telecargar dins « distribucion ».

Aqueste programa es un logicial liure ; lo podètz tornar distribuir
e/o lo modificar segon los tèrmes de la licéncia publica generala GNU,
coma es publicada per la Free Software Foundation ; version 2 de la licéncia,
o (se volètz) tota version seguenta.

Aqueste programa es distribuit en esperant que vos serà util 
mas SENS CAP DE GARANTIDA ; sens tanpauc la garantida implicita
de VALOR MERCANDA o D'ADEQÜACION A UN BESONH PARTICULAR.
Consultatz la licéncia publica generala GNU per mai d'entresenhas.

Deuriatz aver recebut una còpia de la Licéncia Publica Generala GNU
al meteis temps qu’aqueste programa ; s'es pas lo cas, escrivètz a la Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
